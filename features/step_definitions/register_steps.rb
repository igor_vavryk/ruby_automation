Given(/^I open the Register page$/) do
  visit 'https://testautomate.me/redmine/account/register'
end

When(/^I fill all mandatory fields on the Register page$/) do
  find_field(id: 'user_login').send_keys("Test_login_#{SecureRandom.uuid}")
  find_field(id: 'user_password').send_keys('P@SSWORD')
  find_field(id: 'user_password_confirmation').send_keys('P@SSWORD')
  find_field(id: 'user_firstname').send_keys('Test_user_first_name')
  find_field(id: 'user_lastname').send_keys('Test_user_last_name')
  find_field(id: 'user_mail').send_keys("test_email_#{SecureRandom.uuid}@test.test")
  sleep 5
end

And(/^I press the Submit button$/) do
  find_button(value: 'Submit').click
end

Then(/^my account is registered$/) do
  expect(page).to have_content 'Your account has been activated. You can now log in.'
  sleep 5
end