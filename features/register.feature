Feature: Registering of a new user

  Scenario: Register a new user
    Given I open the Register page
    When I fill all mandatory fields on the Register page
    And I press the Submit button
    Then my account is registered
